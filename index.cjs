const got = require("./data-1.cjs");

// - 1. Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.

const countAllPeople = () => {
  const houses = got.houses;
  const people = houses.reduce((acc, house) => {
    return acc + house.people.length;
  }, 0);
  return people;
};

// console.log(countAllPeople());

// - 2. Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.

const peopleByHouses = () => {
  const houses = got.houses;
  const peopleByHouse = houses.reduce((acc, house) => {
    acc[house.name] = house.people.length;
    return acc;
  }, {});
  return peopleByHouse;
};

// console.log(peopleByHouses());

// - 3. Write a function called `everyone` which returns a array of names of all the people in `got` variable.

const everyone = () => {
  const houses = got.houses;
  const names = [];
  houses.forEach((house) => {
    const people = house.people;
    people.forEach((person) => {
      names.push(person.name);
    });
  });
  return names;
};

// console.log(everyone());

// - 4. Write a function called `nameWithS` which returns a array of names of all the people in `got` variable whose name includes `s` or `S`.

const nameWithS = () => {
  const houses = got.houses;
  let ans = [];
  houses.forEach((house) => {
    const people = house.people
      .filter((person) => {
        const name = person.name;
        return name.includes("s") || name.includes("S");
      })
      .map((person) => person.name);
    ans = ans.concat(people);
  });
  return ans;
};

// console.log(nameWithS());

// - 5. Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.

const nameWithA = () => {
  const houses = got.houses;
  let ans = [];
  houses.forEach((house) => {
    const people = house.people
      .filter((person) => {
        const name = person.name;
        return name.includes("a") || name.includes("A");
      })
      .map((person) => person.name);
    ans = ans.concat(people);
  });
  return ans;
};

// console.log(nameWithA());

// - 6. Write a function called `surnameWithS` which returns a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).

const surnameWithS = () => {
  const houses = got.houses;
  let ans = [];
  houses.forEach((house) => {
    const people = house.people
      .filter((person) => {
        const surname = person.name.split(" ")[1];
        return surname.startsWith("S");
      })
      .map((person) => person.name);
    ans = ans.concat(people);
  });
  return ans;
};

// console.log(surnameWithS());

// - 7. Write a function called `surnameWithA` which returns a array of names of all the people in `got` variable whoes surname is starting with `A`(capital a).

const surnameWithA = () => {
  const houses = got.houses;
  let ans = [];
  houses.forEach((house) => {
    const people = house.people
      .filter((person) => {
        const surname = person.name.split(" ")[1];
        return surname.startsWith("A");
      })
      .map((person) => person.name);
    ans = ans.concat(people);
  });
  return ans;
};

// console.log(surnameWithA());

// - 8. Write a function called `peopleNameOfAllHouses` which returns an object with the key of the name of house and value will be all the people in the house in an array.

const peopleNameOfAllHouses = () => {
  const houses = got.houses;
  let ans = {};
  houses.forEach((house) => {
    const houseName = house.name;
    ans[house.name] = house.people.map((person) => person.name);
  });
  return ans;
};

// console.log(peopleNameOfAllHouses());
